import { Component, OnInit,Input} from '@angular/core';
import{PreregistroService} from '../servicios/preregistro.service';


@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {

  listausuarios=[];
  constructor(private getusers:PreregistroService) { }

  ngOnInit(): void {
    this.getallusers();
  }
  getallusers(){
    this.getusers.getallpre().subscribe(
     (data)=>{

      console.log('Data',data);
      
      this.listausuarios=data['data'];

     },
     (error)=>{
       console.error('Fallo en la comunicacion')
     }


   );
  }

  deleteuser(iduser){
    this.getusers.deleteuser(iduser).subscribe(
      
    );
    alert("Usuario eliminado");

  }

}
