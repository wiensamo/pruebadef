import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { homeComponent } from './home/home.component';
import { CargarComponent } from './cargar/cargar.component';
import { LoginComponent } from './login/login.component';
import { PosterListComponent } from './poster-list/poster-list.component';

import { RegistroComponent } from './registro/registro.component';
import{AdministradorComponent} from './administrador/administrador.component';



const routes: Routes = [
{path:'', redirectTo:'/login',pathMatch:'full'},
{path:'Posters', component: homeComponent},
{path:'cargar-componente', component:CargarComponent},
{path:'login',component:LoginComponent},
{path:'registro',component:RegistroComponent},
{path:'admin',component:AdministradorComponent},
{path:'perfil', component: PosterListComponent}

  
];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
