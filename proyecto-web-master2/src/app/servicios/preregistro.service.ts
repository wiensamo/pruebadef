import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PreregistroService {

 url = environment.url;
 url2 = environment.url2;

  constructor(private http: HttpClient) { }


  postallpre(information){
    return this.http.post(this.url,information);

  }
  getallpre(){
    return this.http.get(this.url);
  }
  
  deleteuser(iduser){
    return this.http.delete(this.url2+iduser);
  }

}
