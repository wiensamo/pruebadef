import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule}from '@angular/common/http';

import {usuario} from '../Modils/Usuario.model'

@Injectable({
    providedIn: 'root'
  })
  export class UsuarioServices {

    API_URL ='http://localhost:3000/';
   
  
    constructor(private  http: HttpClient) { }
    
    getuser(){
      return this.http.get('${this.API_URL}/User');
    }
    getUser(idUser: number){
      return this.http.get('$this.API_URL}/User/{idUser}');
    }
    saveUser(usuario: usuario){
      return this.http.post('${this.API_URL}/User/', usuario);
    }
    deleteser(idUser: number){
      return this.http.delete('${this.API_URL}/User/{idUser}')
    }
  
    
    updateUser(idUser, nombres, apellidos, correo) {
      const obj = {
        nombres,
        apellidos,
        correo
      };
      this
        .http
        .post(`${this.API_URL}/update/${idUser}`, obj)
        .subscribe(res => console.log('Done'));
  }
    
  
  
  }
  