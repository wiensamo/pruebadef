import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule}from '@angular/common/http';

import {afiche}  from '../Modils/Afiche.models';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PosterService {

  API_URL ='http://localhost:3000/';
 

  constructor(private  http: HttpClient) { }
  getposters(){
    return this.http.get('${this.API_URL}/poster');
  }
  getPoster(id: number){
    return this.http.get('$this.API_URL}/poster/{idIdAfiche}');
  }
  saveposter(afiche: afiche){
    return this.http.post('${this.API_URL}/poster/', afiche);
  }
  deleteposter(id: number){
    return this.http.delete('${this.API_URL}/poster/{idIdAfiche}')
  }

  
  updateProduct(IdAfiche, titulo, autor) {
    const obj = {
      titulo,
      autor
    };
    this
      .http
      .post(`${this.API_URL}/update/${IdAfiche}`, obj)
      .subscribe(res => console.log('Done'));
}
  


}
